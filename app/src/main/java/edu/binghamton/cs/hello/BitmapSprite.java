package edu.binghamton.cs.hello;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Paint;

public class BitmapSprite extends Sprite {
    protected int resource;
    public Rect screen, mask;
    protected Bitmap bitmap;
    protected int width, height;
    protected int frame_width, frame_height;
    protected int xFrames;
    protected int frame;
    protected ResourceManager res;

    public BitmapSprite(Context context, ResourceManager res, int resource) {
        super(context);
        this.resource = resource;
        this.res = res;

        this.bitmap = res.bitmap(resource);
        this.frame_resize(this.bitmap.getWidth(), this.bitmap.getHeight());
        this.resize(this.frame_width, this.frame_height);
    }

    public BitmapSprite(Context context, ResourceManager res, int resource, int width, int height) {
        this(context, res, resource);

        this.frame_resize(width, height);
        this.resize(width, height);
    }

    @Override
    public void draw(Canvas canvas) {
        if (this.rotation != 0) {
            canvas.save();
            canvas.rotate(this.rotation, this.screen.centerX(), this.screen.centerY());
        }
        canvas.drawBitmap(this.bitmap, this.mask, this.screen, this.paint);
        if (this.rotation != 0) {
            canvas.restore();
        }
    }

    @Override
    public void move(float x, float y) {
        super.move(x, y);
        this.screen.offsetTo((int) x, (int) y);
    }

    private void updateGeometry() {
        this.screen = new Rect((int) this.x, (int) this.y, (int) (this.x + this.width), (int) (this.y + this.height));
        this.xFrames = this.bitmap.getWidth() / this.frame_width;
        this.frame(this.frame);
    }

    public void frame(int frame) {
        this.frame = frame;
        int frameX = (this.frame % this.xFrames) * this.frame_width;
        int frameY = (this.frame / this.xFrames) * this.frame_height;
        this.mask = new Rect(frameX, frameY, frameX + this.frame_width, frameY + this.frame_height);
    }

    public int frame() {
        return this.frame;
    }

    public int resource() {
        return this.resource;
    }

    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
        this.updateGeometry();
    }

    public int width() {
        return this.width;
    }

    public int height() {
        return this.height;
    }

    public void frame_resize(int width, int height) {
        this.frame_width = width;
        this.frame_height = height;
        this.updateGeometry();
    }

    public int frame_width() {
        return this.frame_width;
    }

    public int frame_height() {
        return this.frame_height;
    }

    public boolean intersect(BitmapSprite other) {
        return this.screen.intersect(other.screen);
    }

    public boolean contains(int x, int y) {
        return (x >= this.x && x <= this.x + this.width)
                && (y >= this.y && y <= this.y + this.height);
    }
}
