package edu.binghamton.cs.hello;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Random;

public class GameView extends View {
    final static int BUILDING_SPAWN_RATE = 1100;
    final static int BUILDING_SPAWN_VAR = 500;
    final static int BUILDING_HEIGHT_VAR = 300;
    final static int BAX_FRAME_TICK = 4;
    final static int BAX_FRAMES = 9;
    final static float START_GAME_SPEED = 10;
    final static float GAME_SPEED_FACTOR = 1.1f;
    final static float GAME_SPEED_UP = 600;
    final static float GRAVITY = 1;
    final static float JUMP_STRENGTH = 20;
    final static float EXTRA_JUMP = 0.75f;
    final static int NUM_JUMPS = 30;
    final static int STATE_PAUSE = 0;
    final static int STATE_PLAY = 1;
    final static int STATE_SPLAT = 2;
    private Timer mTimer;

    Context context;
    int ticks;
    int ticksBuildings;
    float dy;
    float game_speed;
    boolean grounded;
    int state;
    FontSprite banner;
    int jumps;
    boolean holding;
    CopyOnWriteArrayList<BitmapSprite> buildings;
    BitmapSprite bax;
    ResourceManager res;
    int building_gap;
    int height_gap;
    Random rand;

    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        this.context = context;
        this.res = new ResourceManager(context);

        this.rand = new Random();
        this.bax = new BitmapSprite(context, this.res, R.drawable.baxter, 128, 176);
        this.bax.frame_resize(42, 64);
        this.banner = new FontSprite(this.context, "Binghamton Parkour");

        this.reset();
        this.state = STATE_PAUSE; // start paused

        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                GameView.this.tick();
                GameView.this.postInvalidate();
            }
        }, 0, 16);
    }

    void tick() {
        if (getHeight() <= 0) {
            return;
        }
        if(this.state == STATE_PAUSE) {
            this.banner.move(getWidth()/2, 300);
        }
        else if(this.state == STATE_PLAY) {
            this.ticks++;
            this.ticksBuildings++;

            if(this.ticks % BAX_FRAME_TICK == 0) {
                this.bax.frame((this.bax.frame() + 1) % BAX_FRAMES);
            }

            if(ticks % GAME_SPEED_UP == 0) {
                this.game_speed *= GAME_SPEED_FACTOR;
            }

            if (this.ticksBuildings % ((BUILDING_SPAWN_RATE + this.building_gap) / (int)game_speed) == 0) {
                BitmapSprite b = new BitmapSprite(this.context, this.res, R.drawable.brick, 800, 400 + this.height_gap);
                b.x(getWidth());
                b.y(getHeight() - b.height());
                this.buildings.add(b);
                this.ticksBuildings = 0;
                this.building_gap = this.rand.nextInt(BUILDING_SPAWN_VAR);
                this.height_gap = this.rand.nextInt(BUILDING_HEIGHT_VAR);
            }

            if (this.holding && this.jumps > 0) {
                this.jumps -= 1;
                this.dy += EXTRA_JUMP;
            }

            this.dy -= GRAVITY;
            this.grounded = false;
            this.bax.dy(-this.dy);


            for (BitmapSprite b : this.buildings) {
                b.dx(-this.game_speed);

                double bx = b.x() + b.width()/2;
                double by = b.y() + b.height()/2;
                double left_angle = Math.atan2(b.y() - by, b.x() - bax.width() - bx);
                double right_angle = Math.atan2(b.y() - by, b.x() + b.width() + bax.width() - bx);
                double bax_angle = Math.atan2(bax.y() + bax.height() - by, bax.x() + bax.width()/2 - bx);

                // no fucking clue why these need to be copied
                // but if they aren't, it deletes the building
                Rect screen = new Rect(b.screen);
                Rect bax_screen = new Rect(this.bax.screen);
                if(screen.intersect(bax_screen)) {
                    if(left_angle < bax_angle && bax_angle < right_angle) {
                        this.dy = 0;
                        this.bax.y(b.y() - this.bax.height());
                        this.grounded = true;
                        this.jumps = NUM_JUMPS;
                    }
                    else  {
                        this.state = STATE_SPLAT;
                    }
                }

                if (b.x() + b.width() < 0) {
                    this.buildings.remove(b);
                }
            }

            if (getHeight() < this.bax.y()) {
                this.end_game();
            }
        }
        else if(this.state == STATE_SPLAT) {
            this.bax.dx(-this.game_speed);
            for (BitmapSprite b : this.buildings) {
                b.dx(-this.game_speed);
            }
            if(this.bax.x() + this.bax.width() < 0) {
                this.end_game();
            }
        }
    }

    public void end_game() {
        this.bax.y(120);
        this.dy = 0;
        this.state = STATE_PAUSE;
        this.banner.text("Game Over");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        if (action == MotionEvent.ACTION_DOWN) {
            if(this.state == STATE_PAUSE) {
                this.reset();
            }
            else if(this.state == STATE_PLAY) {
                if (this.grounded) {
                    this.grounded = false;
                    this.holding = true;
                    this.dy = JUMP_STRENGTH;
                }
            }
        } else if (action == MotionEvent.ACTION_UP) {
            if(this.state == STATE_PLAY) {
                this.holding = false;
            }
        }
        return true;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (this.state == STATE_PAUSE) {
            this.banner.draw(canvas);
        }
        else {
            for (BitmapSprite b : this.buildings) {
                b.draw(canvas);
            }

            this.bax.draw(canvas);
        }
    }

    void reset() {
        this.ticks = 0;
        this.ticksBuildings = 0;
        this.dy = 0;
        this.grounded = false;
        this.state = STATE_PLAY;
        this.jumps = NUM_JUMPS;
        this.holding = false;
        this.game_speed = START_GAME_SPEED;
        this.building_gap = 0;
        this.height_gap = 0;

        this.bax.move(120, 120);

        this.buildings = new CopyOnWriteArrayList<>();
        BitmapSprite b = new BitmapSprite(this.context, this.res, R.drawable.brick, 1200, 400);
        b.x(0);
        b.y(700);
        this.buildings.add(b);

        b = new BitmapSprite(this.context, this.res, R.drawable.brick, 1200, 400);
        b.x(1200);
        b.y(700);
        this.buildings.add(b);
    }
}
