# README #

Binghamton Free Running

Parkour style android game where you have to run and jump from building top to building top.

Binghamton Theme: You play as Baxter, running on top of Binghamton buildings with the university clock tower/library tower in the background.  

Name: Matthew Williamson
Email: mwilli20@binghamton.edu

Name: John Weachock
Email: jweacho1@binghamton.edu